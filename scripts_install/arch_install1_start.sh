#!/bin/sh

# Format partitions
mkfs.vfat -F32 /dev/sda6
mkfs.ext4 /dev/sda7

# Mount partitions
mount /dev/sda7 /mnt
mkdir -p /mnt/boot/EFI && mount -t vfat /dev/sda6 /mnt/boot/EFI

# Prepare CHROOT
pacstrap /mnt base base-devel linux-lts linux-firmware vim
genfstab -U -p /mnt >> /mnt/etc/fstab

# START CHROOT
echo "START CHROOT"
arch-chroot /mnt


