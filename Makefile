########################
#     APPLICATIONS     #
########################

# Main Apps
Terminal = alacritty
Editor = neovim-nightly-bin python-pip nodejs npm
Browser = firefox
FileManager = ranger ueberzug
#Archiver = xarchiver
PdfViewer = zathura zathura-pdf-poppler
VideoViewer = mpv
Screenshot = flameshot dbus

# System
Sysinfos = neofetch
Sysmonitor = htop

# WM
Bar = polybar
Compositor = picom-ibhagwan-git
Launcher = rofi
Wallpaper = feh

# Xorg
Gpu = xf86-video-amdgpu
Xorg = xorg-server xorg-xinit xorg-xrandr xsel
X11 = $(Gpu) $(Xorg)

# Audio
Audio = alsa-utils pulseaudio-alsa

# Archives
Archives = gzip zip unzip unrar

# Fileypes
Filetypes = gvfs exfat-utils ntfs-3g udisks2

# Network
Network = net-tools

# Office
Office = libreoffice-still

# AVR (Personnal because I'm programming AVR uControlers)
AVR = avr-gcc avr-libc avrdude avr-gdb

#####################
#     VARIABLES     #
#####################

MAIN_APPS = $(Terminal) $(Editor) $(Browser) $(FileManager) $(Archiver) $(PdfViewer) $(VideoViewer) $(Screenshot)
SYSTEM = $(Sysinfos) $(Sysmonitor)
WM = $(Bar) $(Compositor) $(Launcher) $(Wallpaper)
XORG = $(Gpu) $(Xorg) $(X11)
MISC = $(Audio) $(Archives) $(Filetypes) $(Network) $(Office)

# Set your misc apps
APPS = discord spotify signal-desktop $(AVR)

# All packages to install
ALL = $(MAIN_APPS) $(SYSTEM) $(WM) $(XORG) $(MISC) $(APPS)


########################
#     INSTALLATION     #
########################

all: update install wm

.PHONY: update install

update:
	yay -Syy

install:
	yay -S $(ALL)

wm:
	yay -S bspwm sxhkd bsp-layout man-db


###############
#     YAY     #
###############

yay:
	git clone https://aur.archlinux.org/yay.git


###############
#     ZSH     #
###############

# Set your username
USER = plunne

zsh: zsh-omz

zsh-omz:
	sudo pacman -S zsh
	chsh -s /bin/zsh $(USER)
	echo "exec zsh" > .bashrc
	sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

p10k:
	git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k


################
#     NVIM     #
################

nvim: nvim-install nvim-servers

nvim-install:
	pip install pynvim
	sudo npm i -g neovim

nvim-servers: nvim-nodejs nvim-python

nvim-nodejs:
	sudo npm install -g $(BASH) $(CSS) $(HTML) $(JSON) $(SQL) $(VIM) $(YAML)

BASH = bash-language-server
CSS = vscode-css-languageserver-bin
HTML = vscode-html-languageserver-bin
JSON = vscode-json-languageserver
SQL = sql-language-server
VIM = vim-language-server
YAML = yaml-language-server

nvim-python:
	pip install $(PY)

PY = python-language-server


##################
#     RANGER     #
##################

ranger:
	git clone https://github.com/alexanderjeurissen/ranger_devicons ~/.config/ranger/plugins/ranger_devicons
